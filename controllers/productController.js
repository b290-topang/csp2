// Setting up the Dependencies 
	const bcrypt = require("bcrypt");
	const auth = require("../auth.js");

// Models
	const User = require("../models/User.js");
	const Product = require("../models/Product.js");
	const Order = require("../models/Order.js");

// Module Exports
// Create Product
	module.exports.productRegister = (data) => {
		let newProduct = new Product ({
			name: data.name,
			description: data.description,
			price: data.price,
		})

		return newProduct.save().then(product => product).catch(error => false)
	}

// View ONE Product
	module.exports.productViewOne = (data) => {
		return Product.findById({ _id: data.id }).then(product => {
			if (product == null) {
				return false
			} else {
				return product
			}

		}).catch(error => {
			return console.log(error) })
	}

// View ALL Product
	module.exports.productViewAll = () => {
		return Product.find().then(product => {
			if(product.length == 0) {
				return false
			} else {
				return product
			}
		}).catch(error => false)
	}

// View ALL TRUE Product
	module.exports.productViewAllActive = () => {
		return Product.find({ isActive: true }).then(product => {
			if(product.length == 0) {
				return false
			} else {
				return product
			}
		}).catch(error => false)
	}

// View ALL FALSE Product
	module.exports.productViewAllNotActive = () => {
		return Product.find({ isActive: false }).then(product => {
			if(product.length == 0) {
				return false
			} else {
				return product
			}
		}).catch(error => false)
	}

// Update Product
	module.exports.productUpdate = (dataParams, dataBody) => {
		let updatedProduct = {
			name: dataBody.name,
			description: dataBody.description,
			price: dataBody.price
		}
		return Product.findByIdAndUpdate(dataParams.productId, updatedProduct, { new: true }).then(product => product).catch(error => false)
	}

// Archive Product
	module.exports.productArchive = (dataParams) => {
		return Product.findByIdAndUpdate(dataParams.productId, { isActive: false }, { new: true }).then(product => {
			if (product == null) {
				return false
			} else {
				return product
			}
		}).catch(error => { return false })
	}

// Activate Product
	module.exports.productActivate = (dataParams) => {
		return Product.findByIdAndUpdate(dataParams.productId, { isActive: true }, { new: true }).then(product => {
			if (product == null) {
				return false
			} else {
				return product
			}
		}).catch(error => { return false })
	}

//
//
//
//
//
// Delete ALL Products
	module.exports.productDelete = () => {
		return Product.deleteMany().then(deleteProduct => deleteProduct).catch(error => false)

	}