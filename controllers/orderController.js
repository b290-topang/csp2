// Setting up the Dependencies 
	const bcrypt = require("bcrypt");
	const auth = require("../auth.js");

// Models
	const User = require("../models/User.js");
	const Product = require("../models/Product.js");
	const Order = require("../models/Order.js");

// Module Exports
// View Auth User Order
	module.exports.orderView = (dataBody) => {
		return Order.find( {userId: dataBody.userId} ).then(order => order).catch(error => false)
	}
// View ALL Orders
	module.exports.orderViewAll = () => {
		return Order.find().then(order => {
			if (order.length == 0) {
				return false
			} else {
				return order
			}
		}).catch(error => false)
	}
// View User Order
	module.exports.orderViewAdmin = (dataBody) => {
		return Order.find( {userId: dataBody.userId} ).then(order => order).catch(error => false)
	}

// Add to Cart
// Add Products
	let cart = []
	module.exports.orderAdd = async (data) => {

		let itemName = await Product.findById({ _id: data.productId }).then(product => { return product.name} 
		)

		let newItem = {
			productName: itemName,
			productQty: data.quantity
		}

		cart.push(newItem)
		return true
	}

// Change Product Quantity
	module.exports.orderChange = async (data) => {
		let itemName = await Product.findById({ _id: data.productId }).then(product => { return product.name} 
		)

		for (let index = 0; index < cart.length; index++) {
			let itemCheck = cart[index]
		
			if(itemName === itemCheck.productName) {
				itemCheck.productQty = data.quantity

				return cart
			} else {
				return false
			}
		}
	}

// Remove Product from Cart
	module.exports.orderRemove = async (data) => {
		let itemName = await Product.findById({ _id: data.productId }).then(product => { return product.name} 
		)

		for (let index = 0; index < cart.length; index++) {
			let itemCheck = cart[index]

			if(itemName === itemCheck.productName) {
				cart.splice(index, 1)

				return cart
			} else {
				return false
			}
		}
	}

// Subtotal of each item from Cart
	module.exports.orderSubtotal = async (data) => {
		let arrSubtotal = [];
		for (let index = 0; index < cart.length; index++) {
			let itemCheck = cart[index]

			let itemPrice = await Product.find({ name: itemCheck.productName }).then(product => product)

			for (let index = 0; index < itemPrice.length; index++) {
				let getPrice = itemPrice[index]

				let objSubtotal = {
					name: itemCheck.productName,
					quantity: itemCheck.productQty,
					productPrice: getPrice.price,
					subtotal: Number(getPrice.price) * Number(itemCheck.productQty)
				}

				arrSubtotal.push(objSubtotal)
			}
		
		}
		return arrSubtotal
	}

// Total price of items from Cart
	module.exports.orderTotal = async () => {
		let total = 0
		for (let index = 0; index < cart.length; index++) {
			let itemCheck = cart[index]

			let itemPrice = await Product.find({ name: itemCheck.productName }).then(product => product)

			for (let index = 0; index < itemPrice.length; index++) {
				let getPrice = itemPrice[index]
				total += getPrice.price * itemCheck.productQty
			}
		}
		return total.toString();
	}

//
//
//
//
//
// Delete ALL Order
	module.exports.orderDelete = () => {
		return Order.deleteMany().then(deleteOrder => deleteOrder).catch(error => false)

	}
