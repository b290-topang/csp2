// Setting up the Dependencies 
	const bcrypt = require("bcrypt");
	const auth = require("../auth.js");

// Models
	const User = require("../models/User.js");
	const Product = require("../models/Product.js");
	const Order = require("../models/Order.js");

// Module Exports
// Create User
	module.exports.userRegister = (data) => {
		return User.findOne({ email: data.email }).then(user => { 
			if (user == null) {
				let newUser = new User ({
					firstName: data.firstName,
					lastName: data.lastName,
					email: data.email,
					password: bcrypt.hashSync(data.password, 10)
				})

				return newUser.save().then(user => { return true }).catch(error => false)
			} else {
				return false
			}
		}).catch(error => false)
	}

// View ONE User
	module.exports.userView = (data) => {
		return User.findById({ _id: data.userId }).then(user => {
			if (user == null) {
				return false
			} else {
				return user
			}
		}).catch(error => { 
			return false })
}			

// View ALL User
	module.exports.userViewAll = () => {
		return User.find().then(user => {
			if (user.length == 0) {
				return false
			} else {
				return user
			}
		}).catch(error => false)
	}

// Authenticate User
	module.exports.userAuth = (data) => {
		return User.findOne({ email: data.email }).then(result => {
			if(result == null) {
				return false
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

				if (isPasswordCorrect) {
					return { access: auth.createAccessToken(result) }
				} else {
					return false
				}
			}
		})
	}

// Create Order
	module.exports.userCheckOut = async (data, dataId) => {
		let getPrice = await Product.findById({ _id: data.productId }).then(result => {
			return result.price
		})

		let newOrder = new Order ({
			userId: dataId.userId,
			products: [{
				productId: data.productId,
				quantity: data.quantity
			}],
			totalAmount: (getPrice * data.quantity)
		})

		return newOrder.save().then(order => order).catch(error => false)
	}

// Set User as Admin
	module.exports.userSetAdmin = (dataBody) => {
		let setAdmin = {
			isAdmin: dataBody.isAdmin
		}

		return User.findOneAndUpdate({ email: dataBody.email }, setAdmin).then(user => {
			if (user == null) {
				return false
			} else {
				return true
			}

		}).catch(error => false)
	}

// View Auth User Order
	module.exports.userViewAuth = (dataBody) => {
		return Order.find( { userId: dataBody.userId } ).then(order => {
				if (order.length == 0) {
					return false
				} else {
					return order
				}
			}).catch(error => false)
	}

// Update Auth User Password
	module.exports.userChangePassword = (userId, dataBody) => {
		let updatedPassword = {
			password: bcrypt.hashSync(dataBody.password, 10)
		}

		return User.findByIdAndUpdate(userId.userId, updatedPassword, { new: true }).then(user => user).catch(error => { console.log (error) });
	}
//
//
//
//
//
// Delete ALL Users
	module.exports.userDelete = () => {
		return User.deleteMany().then(deleteUser => deleteUser).catch(error => false)

	}