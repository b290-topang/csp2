const express = require("express");
const router = express.Router();

const auth = require("../auth.js")
const userController = require("../controllers/userController.js");
const productController = require("../controllers/productController.js");
const orderController = require("../controllers/orderController.js");

// Routers
// Create Product
	router.post("/register", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			productController.productRegister(request.body).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})
// View ONE Product
	router.post("/view", (request, response) => {
		productController.productViewOne(request.body).then(result => response.send(result))
	})

// View ALL Product
	router.get("/viewAll", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin){
			productController.productViewAll().then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// View ALL TRUE Product
	router.get("/viewAllActive", (request, response) => {
		productController.productViewAllActive(request.body).then(result => response.send(result))
		
	})

// View ALL FALSE Product
	router.get("/viewAllNotActive", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin){
			productController.productViewAllNotActive(request.body).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// Update Product
	router.put("/update/:productId", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			productController.productUpdate(request.params, request.body).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// Archive Product
	router.patch("/archive/:productId", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			productController.productArchive(request.params).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// Activate Product
	router.patch("/activate/:productId", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			productController.productActivate(request.params).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

//
//
//
//
//
// Delete ALL Users
	router.delete("/delete", (request, response) => {
		productController.productDelete().then(result => response.send(result))
	})
module.exports = router;