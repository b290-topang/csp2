const express = require("express");
const router = express.Router();

const auth = require("../auth.js")
const userController = require("../controllers/userController.js");
const productController = require("../controllers/productController.js");
const orderController = require("../controllers/orderController.js");

// Routers
// User Registration - /users/register
	router.post("/register", (request, response) => {
		userController.userRegister(request.body).then(result => response.send(result))
	})

// View ONE User
	router.get("/view/", (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}

		userController.userView(data).then(result => response.send(result))
	})
	
// View ALL User
	router.get("/viewAll", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			userController.userViewAll().then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// Authenticate User
	router.post("/auth", (request, response) => {
		userController.userAuth(request.body).then(result => response.send(result))
	})

// Create Order
	router.post("/checkout", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			response.send("User is an admin")
		} else {
			let data = {
				userId: auth.decode(request.headers.authorization).id
			}
			
			userController.userCheckOut(request.body, data).then(result => response.send(result))
		}
	})

// Set User as Admin
	router.patch("/admin/", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			userController.userSetAdmin(request.body).then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}	
	})

// View Auth User Order
	router.post("/viewAuth", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		userController.userViewAuth(data).then(result => response.send(result))

	})

// Update Auth User Password
	router.put("/changepassword", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		userController.userChangePassword(data, request.body).then(result => response.send(result))
	})

//
//
//
//
//
// Delete ALL Users
	router.delete("/delete", (request, response) => {
		userController.userDelete().then(result => response.send(result))
	})

module.exports = router;

