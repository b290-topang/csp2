const express = require("express");
const router = express.Router();

const auth = require("../auth.js")
const userController = require("../controllers/userController.js");
const productController = require("../controllers/productController.js");
const orderController = require("../controllers/orderController.js");

// Routers
// View Auth User Order
	router.get("/view", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}

		orderController.orderView(data).then(result => response.send(result))
	})

// View ALL Orders
	router.get("/viewAll", auth.verify, (request, response) => {
		const isAdmin = auth.decode(request.headers.authorization).isAdmin

		if (isAdmin) {
			orderController.orderViewAll().then(result => response.send(result))
		} else {
			response.send("User is not an admin")
		}
	})

// View User Order
	router.post("/viewAdmin", auth.verify, (request, response) => {
		orderController.orderViewAdmin(request.body).then(result => response.send(result))
	})

// Add to Cart
// Add Product to Cart
	router.post("/cart/add", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		
		orderController.orderAdd(request.body, data).then(result => response.send(result))
	})

// Change Product Quantity
	router.post("/cart/edit", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		
		orderController.orderChange(request.body).then(result => response.send(result))
	})

// Remove Product from Cart
	router.post("/cart/remove", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		
		orderController.orderRemove(request.body).then(result => response.send(result))
	})

// Subtotal of each item from Cart
	router.get("/cart/viewcart", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		
		orderController.orderSubtotal().then(result => response.send(result))
	})

// Total price of items from Cart
	router.get("/cart/checkout/", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		
		orderController.orderTotal().then(result => response.send(result))
	})

//
//
//
//
//
// Delete ALL Users
	router.delete("/delete", (request, response) => {
		orderController.orderDelete().then(result => response.send(result))
	})

module.exports = router;

