// Setting up the Dependencies
	const express = require("express");
	const mongoose = require("mongoose");

	const cors = require("cors");

// Routes
	const userRoute = require("./routes/userRoute.js");
	const productRoute = require("./routes/productRoute.js");
	const orderRoute = require("./routes/orderRoute.js");

// Setting up the Server
	const app = express();

// Connecting the Database
	mongoose.connect("mongodb+srv://admin:admin123@zuitt.grs8r9i.mongodb.net/e-commerce?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });
	mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));

// Setting up the Middlewares
// Allows all resources to access our backend application
	app.use(cors());
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));

// Routes
	app.use("/users", userRoute);
	app.use("/products", productRoute);
	app.use("/orders", orderRoute);

	if(require.main === module) {
		app.listen(process.env.PORT || 4000, () => { 
			console.log(`API is now online on port ${ process.env.PORT || 4000 }`) 
		});
	}
	module.exports = { app, mongoose };