// Setting up the dependencies
	const jwt = require("jsonwebtoken");
	const secret = "ECommerceAPI0606";

// Token Creation
	module.exports.createAccessToken = (user) => {
		const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {})
}

// Token Verification
	module.exports.verify = (req, res, next) => {
		let token = req.headers.authorization

		if(typeof token !== "undefined") { 
			token = token.slice(7, token.length);

			return jwt.verify(token, secret, (err, data) => {
				if (err) {
					return res.send({ auth: "Not an authenticated user" });
				} else {
					next();
				}
			})

		} else {
		return res.send({ auth: "Not an authenticated user" });
	}
}

// Token Decryption

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, { complete: true }).payload;
			}
		})

	} else {
		return null
	}
}