// Setting up the Dependencies
	const express = require("express");
	const mongoose = require("mongoose");

// User Model
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "First name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		}
	})

module.exports = mongoose.model("User", userSchema)