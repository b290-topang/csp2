// Setting up the Dependencies
	const express = require("express");
	const mongoose = require("mongoose");

// Product Model
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Product name is required"]
		},
		description: {
			type: String,
			required: [true, "Product description is required"]
		},
		price: {
			type: Number,
			required: [true, "Product name is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	})

module.exports = mongoose.model("Product", productSchema)