// Setting up the Dependencies
	const express = require("express");
	const mongoose = require("mongoose");

// Order Model

	const orderSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		products: [
			{
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				quantity: {
					type: Number,
					required: [true, "Product quantity is required"]
				}
			}
		],
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	})

module.exports = mongoose.model("Order", orderSchema)